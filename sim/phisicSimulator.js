class PhisicSimulator {
  entityKeys = [
    'can',
    'root',
    'pot',
    'heater',
    'phUpDispencer',
    'phDownDispencer',
    'relPressInDispencer',
    'relPressOutDispencer',
    'fertDispencer',
    'heaterSensor',
    'phSensor',
    'fertSensor',
  ];
  phisicMap = {
    thermo: null,
    relPress: null,
    flowX: null,
    flowY: null,
  };

  metricSetup = {
    fieldSize: {
      width: 32,
      height: 16,
    },
    kernelHalfSize: {
      thermo: 3,
      relPress: 1,
      shiftingThermo: 3,
    },
    entityRects: {
      can: [{
        coords: {
          x: 0,
          y: 0,
        },
        size: {
          width: 12,
          height: 16,
        }
      },
      {
        coords: {
          x: 16,
          y: 0,
        },
        size: {
          width: 12,
          height: 16,
        }
      },
      {
        coords: {
          x: 12,
          y: 5,
        },
        size: {
          width: 4,
          height: 1,
        }
      },
      {
        coords: {
          x: 12,
          y: 12,
        },
        size: {
          width: 4,
          height: 1,
        }
      }],
      root: [{
        coords: {
          x: 3,
          y: 0,
        },
        size: {
          width: 6,
          height: 12,
        }
      }],
      pot: [{
        coords: {
          x: 4,
          y: 0,
        },
        size: {
          width: 4,
          height: 4,
        }
      }],
      heater: [{
        coords: {
          x: 20,
          y: 14
        },
        size: {
          width: 4,
          height: 1,
        }
      }],
      phUpDispencer: [{
        coords: {
          x: 11,
          y: 2,
        },
        size: {
          width: 1,
          height: 1,
        }
      }],
      phDownDispencer: [{
        coords: {
          x: 11,
          y: 4,
        },
        size: {
          width: 1,
          height: 1,
        }
      },
    ],
      relPressInDispencer: [{
        coords: {
          x: 14,
          y: 5,
        },
        size: {
          width: 1,
          height: 1,
        }
      },
      {
        coords: {
          x: 13,
          y: 12,
        },
        size: {
          width: 1,
          height: 1,
        }
      }],
      relPressOutDispencer: [{
        coords: {
          x: 13,
          y: 5,
        },
        size: {
          width: 1,
          height: 1,
        }
      },
      {
        coords: {
          x: 14,
          y: 12,
        },
        size: {
          width: 1,
          height: 1,
        }
      }],
      fertDispencer: [{
        coords: {
          x: 11,
          y: 6
        },
        size: {
          width: 1,
          height: 1
        }
      }],
      heaterSensor: [{
        coords: {
          x: 0,
          y: 3
        },
        size: {
          width: 1,
          height: 1
        }
      }],
      phSensor: [{
        coords: {
          x: 0,
          y: 5,
        },
        size: {
          width: 1,
          height: 1,
        }
      }],
      fertSensor: [{
        coords: {
          x: 0,
          y: 7
        },
        size: {
          width: 1,
          height: 1,
        }
      }],
    },
  }

  dt = 1/60;
  timeFactor = 1;
  resolutionFactor = 2;
  turbulence = 1;

  tweakable = {
    heaterPower: 0,
    relPressPower: 0,
    temperatureKelOutside: 272 + 16, // in K
    phConcentration: 1,
    phUpDispencePower: 0,
    phDownDispencePower: 0,
    fertConcentration: 1,
    fertDispencePower: 0,
  }
  statistic = {
    lastTs: null,
    cycleDt: null,
    dutyDt: null,
    smooth: {
      cycleDt: 0,
      dutyDt: 0,
    }
  }
  // cached precalc at init
  phisicCache = {
    fieldSize: { // phisic block
      width: this.metricSetup.fieldSize.width * this.resolutionFactor,
      height: this.metricSetup.fieldSize.height * this.resolutionFactor
    },
    kernelHalfSize: {
      thermo: this.metricSetup.kernelHalfSize.thermo * this.resolutionFactor,
      relPress: this.metricSetup.kernelHalfSize.relPress * this.resolutionFactor,
      shiftingThermo: this.metricSetup.kernelHalfSize.shiftingThermo * this.resolutionFactor,
    },
    entityRects: null,
    canBlocks: null,
  }

  getMapId(phisicCoords, channels = 1) {
    return (phisicCoords.y * this.phisicCache.fieldSize.width + phisicCoords.x) * channels;
  }

  precalcPhisicEntityRects() {
    const keyedEntries = Object.entries(this.metricSetup.entityRects).map(([entityRectsKey, metricRects]) => {
      const phisicRects = metricRects.map((metricRect) => {
        return {
          coords:{
            x: metricRect.coords.x * this.resolutionFactor,
            y: metricRect.coords.y * this.resolutionFactor,
          },
            size:{
            width: metricRect.size.width * this.resolutionFactor,
            height: metricRect.size.height * this.resolutionFactor,
          }
        }
      });
      return [entityRectsKey, phisicRects]
    });
    const phisicEntityRects = Object.fromEntries(keyedEntries);

    this.phisicCache.entityRects = phisicEntityRects;
  }
  checkPointEntity(phisicCoords, entityRects) {
    const inRectId = entityRects.findIndex((entityRect) => {
      return phisicCoords.x >= entityRect.coords.x && phisicCoords.x < (entityRect.coords.x + entityRect.size.width) && phisicCoords.y >= entityRect.coords.y && phisicCoords.y < (entityRect.size.height + entityRect.coords.y);;
    });
    const isInEntity = (inRectId !== -1);
    return isInEntity;
  }
  getPointEntityState(phisicCoords) {
    const state = Object.entries(this.phisicCache.entityRects).reduce((state, [entityKey, entityRects]) => {
      state[entityKey] = this.checkPointEntity(phisicCoords, entityRects);
      return state;
    }, {});
    return state;
  }
  precalcPhisicCanBlocks() {
    const canBlocks = [];
    this.phisicCache.entityRects.can.forEach((canRect)=>{
      for (let cY = canRect.coords.y; cY < canRect.coords.y + canRect.size.height; cY++) {
        for (let cX = canRect.coords.x; cX < canRect.coords.x + canRect.size.width; cX++) {
          const coords = {
            x: cX,
            y: cY
          };
          const entityState = this.getPointEntityState(coords);
          const mapId = this.getMapId(coords);
          const canBlock = {coords, mapId, entityState, kernelMask: null, deltaCross: null};
          canBlocks.push(canBlock);
        }
      }
    })
    this.phisicCache.canBlocks = canBlocks;
  }
  precalcPhisicCanBlockKernelMasks(kernelHalfSizes) {
    this.phisicCache.canBlocks.forEach((canBlock) => {
      canBlock.kernelMask = {};
      kernelHalfSizes.forEach((kernelHalfSize) => {
        const kernelSize = ((kernelHalfSize * 2) + 1);
        const kernelMask = [];
        for (let kY = -kernelHalfSize; kY <= kernelHalfSize; kY++) {
          for (let kX = -kernelHalfSize; kX <= kernelHalfSize; kX++) {
            const kernelCoords = {
              x: kX + kernelHalfSize,
              y: kY + kernelHalfSize,
            };
            const phisicCoords = {
              x: kX + canBlock.coords.x,
              y: kY + canBlock.coords.y,
            }
            const kernelId = kernelCoords.y * kernelSize + kernelCoords.x;
            if (this.checkPointEntity(phisicCoords, this.phisicCache.entityRects.can)) {
              const mapId = this.getMapId(phisicCoords);
              let kernelMaskElement = {
                kernelId,
                mapId,
              }
              kernelMask.push(kernelMaskElement);
            }

          }
        }

        canBlock.kernelMask[kernelHalfSize] = kernelMask;
      });
    });
  }
  precalcPhisicCanBlockDeltaCross() {
    this.phisicCache.canBlocks.forEach((canBlock) => {
      const deltaCross = {
        mapId: {
          left: canBlock.mapId,
          right: canBlock.mapId,
          top: canBlock.mapId,
          bottom: canBlock.mapId,
        },
        delta: {
          x: 0,
          y: 0,
        }
      };
      { // left
        const phisicCoords = {
          x: canBlock.coords.x - 1,
          y: canBlock.coords.y,
        }
        if (this.checkPointEntity(phisicCoords, this.phisicCache.entityRects.can)) {
          const mapId = this.getMapId(phisicCoords);
          deltaCross.delta.x++;
          deltaCross.mapId.left = mapId;
        }
      }
      { // right
        const phisicCoords = {
          x: canBlock.coords.x + 1,
          y: canBlock.coords.y,
        }
        if (this.checkPointEntity(phisicCoords, this.phisicCache.entityRects.can)) {
          const mapId = this.getMapId(phisicCoords);
          deltaCross.delta.x++;
          deltaCross.mapId.right = mapId;
        }
      }
      { // top
        const phisicCoords = {
          x: canBlock.coords.x,
          y: canBlock.coords.y - 1,
        }
        if (this.checkPointEntity(phisicCoords, this.phisicCache.entityRects.can)) {
          const mapId = this.getMapId(phisicCoords);
          deltaCross.delta.y++;
          deltaCross.mapId.top = mapId;
        }
      }
      { // bottom
        const phisicCoords = {
          x: canBlock.coords.x,
          y: canBlock.coords.y + 1,
        }
        if (this.checkPointEntity(phisicCoords, this.phisicCache.entityRects.can)) {
          const mapId = this.getMapId(phisicCoords);
          deltaCross.delta.y++;
          deltaCross.mapId.bottom = mapId;
        }
      }

      canBlock.deltaCross = deltaCross;

    });
  }
  constructor() {
    this.reset();
    setInterval(()=>{
      this.phisicIteration();
    }, this.dt*1000);
  }
  reset() {
    this.phisicMap.thermo = new Float64Array(this.phisicCache.fieldSize.width*this.phisicCache.fieldSize.height);
    this.phisicMap.relPress = new Float64Array(this.phisicCache.fieldSize.width*this.phisicCache.fieldSize.height);
    this.phisicMap.flowX = new Float64Array(this.phisicCache.fieldSize.width*this.phisicCache.fieldSize.height);
    this.phisicMap.flowY = new Float64Array(this.phisicCache.fieldSize.width*this.phisicCache.fieldSize.height);
    for (let i = 0; i < this.phisicMap.thermo.length; i++) {
      this.phisicMap.thermo[i] = this.tweakable.temperatureKelOutside;
    }
    this.precalcPhisicEntityRects();
    this.precalcPhisicCanBlocks();
    this.precalcPhisicCanBlockKernelMasks(Object.values(this.phisicCache.kernelHalfSize));
    this.precalcPhisicCanBlockDeltaCross();
  }
  updateStatisticAtStart() {
    const now = Date.now();
    this.statistic.cycleDt = now - this.statistic.lastTs;
    this.statistic.lastTs = now;
  }
  updateStatisticAtEnd() {
    const now = Date.now();
    this.statistic.dutyDt = now - this.statistic.lastTs;
    this.statistic.smooth.cycleDt = this.statistic.smooth.cycleDt * 0.9 + this.statistic.cycleDt * 0.1;
    this.statistic.smooth.dutyDt = this.statistic.smooth.dutyDt * 0.9 + this.statistic.dutyDt * 0.1;
  }
  phisicIteration() {
    this.updateStatisticAtStart();
       for (let i = 0; i < this.timeFactor; i++) {
        this.phisicThermoIteration();
      }
      this.updateStatisticAtEnd();
  }

  phisicThermoIteration() {
    this.phisicDiffuseIteration();
    this.phisicThermoDispenceIteration();
    this.phisicThermoRadiateIteration();
    this.phisicRelPressDispenceIteration();
    this.phisicBlurIteration();
    this.phisicCalcFlow();
    this.phisicShiftingBlurIteration();
    //this.phisicWalkIteration();
  }

  phisicDiffuseIteration() {
    const stochoItersPerSec = 1000 * this.resolutionFactor**2;
    const stochoIters = stochoItersPerSec * this.dt;

    const kernelHalfSize = 2 * this.resolutionFactor;

    for (let s = 0; s < stochoIters; s++) {
      const randomCanBlockId = Math.floor(Math.random() * this.phisicCache.canBlocks.length);
      const aCanBlock = this.phisicCache.canBlocks[randomCanBlockId];
      const aCoords = aCanBlock.coords;
      const aMapId = aCanBlock.mapId;
      const bCoords = {
        x: aCoords.x + Math.floor(Math.random() * kernelHalfSize * 2) - kernelHalfSize,
        y: aCoords.y + Math.floor(Math.random() * kernelHalfSize * 2) - kernelHalfSize,
      };
      if ((bCoords.x >= 0) &&
          (bCoords.y >= 0) &&
          (bCoords.x < this.phisicCache.fieldSize.width) &&
          (bCoords.y < this.phisicCache.fieldSize.height) &&
          (this.checkPointEntity(bCoords, this.phisicCache.entityRects.can))) {

        const bMapId = this.getMapId(bCoords);
        {
          const a = this.phisicMap.thermo[aMapId];
          const b = this.phisicMap.thermo[bMapId];
          this.phisicMap.thermo[bMapId] = a;
          this.phisicMap.thermo[aMapId] = b;
        }
        {
          const a = this.phisicMap.relPress[aMapId];
          const b = this.phisicMap.relPress[bMapId];
          this.phisicMap.relPress[bMapId] = a;
          this.phisicMap.relPress[aMapId] = b;
        }
      }
    }

  }
  phisicRelPressDispenceIteration() {
    const relPressInRects = this.phisicCache.entityRects['relPressInDispencer'];
    const relPressOutRects = this.phisicCache.entityRects['relPressOutDispencer'];
    const phisicRelPressInBlocksCoords = [];
    const phisicRelPressOutBlocksCoords = [];
    relPressInRects.forEach((relPressInRect) => {
      for (let cY = relPressInRect.coords.y; cY < relPressInRect.coords.y + relPressInRect.size.height; cY++) {
        for (let cX = relPressInRect.coords.x; cX < relPressInRect.coords.x + relPressInRect.size.width; cX++) {
          const phisicCoords = {
            x: cX,
            y: cY
          };
          phisicRelPressInBlocksCoords.push(phisicCoords);
        }
      }
    });
    relPressOutRects.forEach((relPressOutRect) => {
      for (let cY = relPressOutRect.coords.y; cY < relPressOutRect.coords.y + relPressOutRect.size.height; cY++) {
        for (let cX = relPressOutRect.coords.x; cX < relPressOutRect.coords.x + relPressOutRect.size.width; cX++) {
          const phisicCoords = {
            x: cX,
            y: cY
          };
          phisicRelPressOutBlocksCoords.push(phisicCoords);
        }
      }
    });

    phisicRelPressInBlocksCoords.forEach((phisicRelPressInBlockCoords)=>{
      const mapId = this.getMapId(phisicRelPressInBlockCoords);
      const relPressPowerPerIteration = this.tweakable.relPressPower * this.dt;
      const relPressPowerPerBlock = relPressPowerPerIteration / phisicRelPressInBlocksCoords.length;
      this.phisicMap.relPress[mapId] += relPressPowerPerBlock;
    });
    phisicRelPressOutBlocksCoords.forEach((phisicRelPressInBlockCoords)=>{
      const mapId = this.getMapId(phisicRelPressInBlockCoords);
      const relPressPowerPerIteration = this.tweakable.relPressPower * this.dt;
      const relPressPowerPerBlock = relPressPowerPerIteration / phisicRelPressInBlocksCoords.length;
      this.phisicMap.relPress[mapId] -= relPressPowerPerBlock;
    });
  }
  phisicThermoDispenceIteration() {
    const heaterRects = this.phisicCache.entityRects['heater'];
    const phisicHeaterBlocksCoords = [];
    heaterRects.forEach((heaterRect) => {
      for (let cY = heaterRect.coords.y; cY < heaterRect.coords.y + heaterRect.size.height; cY++) {
        for (let cX = heaterRect.coords.x; cX < heaterRect.coords.x + heaterRect.size.width; cX++) {
          const phisicCoords = {
            x: cX,
            y: cY
          };
          phisicHeaterBlocksCoords.push(phisicCoords);
        }
      }
    });
    phisicHeaterBlocksCoords.forEach((phisicHeaterBlockCoords)=>{
      const mapId = this.getMapId(phisicHeaterBlockCoords);
      const heaterPowerPerIteration = this.tweakable.heaterPower * this.dt;
      const heaterPowerPerBlock = heaterPowerPerIteration / phisicHeaterBlocksCoords.length;
      this.phisicMap.thermo[mapId] += heaterPowerPerBlock;
    });

  }

  phisicBlurIteration() {
    //this.phisicMap.thermo = this.phisicBlurMap(this.phisicMap.thermo, this.phisicCache.kernelHalfSize.thermo);

    this.phisicMap.relPress = this.phisicBlurMap(this.phisicMap.relPress, this.phisicCache.kernelHalfSize.relPress);

  }
  phisicBlurBuildKernel(kernelHalfSize) {
    const kernelSize = ((kernelHalfSize * 2)+1);
    const kernel = new Float64Array(kernelSize**2);
    for (let kY = -kernelHalfSize; kY <= kernelHalfSize; kY++) {
      for (let kX = -kernelHalfSize; kX <= kernelHalfSize; kX++) {
        const kernelCoords = {
          x: kX + kernelHalfSize,
          y: kY + kernelHalfSize,
        };
        const kernelId = kernelCoords.y * kernelSize + kernelCoords.x;
        const kernelWeight = 1 / (1 + Math.sqrt(kX**2 + kY**2));
        kernel[kernelId] = kernelWeight;
      }
    }
    return kernel;
  }
  phisicBlurMap(phisicMap, kernelHalfSize) {
    const bluredMap = new Float64Array(this.phisicCache.fieldSize.width*this.phisicCache.fieldSize.height);
    const kernel = this.phisicBlurBuildKernel(kernelHalfSize);
    this.phisicCache.canBlocks.forEach((canBlock)=>{
      const kernelMask = canBlock.kernelMask[kernelHalfSize];
      let valueSum = 0;
      let samplesWeightSum = 0;
      kernelMask.forEach((kernelMaskElement) => {
        const sampleWeight = kernel[kernelMaskElement.kernelId];
        valueSum += phisicMap[kernelMaskElement.mapId] * sampleWeight;

        samplesWeightSum += sampleWeight;
      });
      const mapCId = canBlock.mapId;
      if (samplesWeightSum) {
        bluredMap[mapCId] = valueSum / samplesWeightSum;
      } else {
        console.warn('no samples!');
        bluredMap[mapCId] = phisicMap[mapCId];
      }
    })
    return bluredMap;
  }


  phisicCalcFlow() {
    this.phisicCache.canBlocks.forEach((canBlock)=>{
      if (canBlock.deltaCross.delta.x > 0) {
        const diff = this.phisicMap.relPress[canBlock.deltaCross.mapId.right] - this.phisicMap.relPress[canBlock.deltaCross.mapId.left];
        this.phisicMap.flowX[canBlock.mapId] = diff / canBlock.deltaCross.delta.x;
      } else {
        this.phisicMap.flowX[canBlock.mapId] = 0;
      }
      if (canBlock.deltaCross.delta.y > 0) {
        const diff = this.phisicMap.relPress[canBlock.deltaCross.mapId.bottom] - this.phisicMap.relPress[canBlock.deltaCross.mapId.top];
        this.phisicMap.flowY[canBlock.mapId] = diff / canBlock.deltaCross.delta.y;
      } else {
        this.phisicMap.flowY[canBlock.mapId] = 0;
      }
    })
  }


  phisicShiftingBlurBuildKernel(kernelHalfSize) {
    const kernelSize = ((kernelHalfSize * 2)+1);
    const kernel = new Float64Array(kernelSize**2);
    return kernel;
  };
  phisicShiftingBlurCalcShiftDeltaCoords(canBlock) {
    const deltaCoords = {
      x: Math.min(2, Math.max(-2, this.phisicMap.flowX[canBlock.mapId] * 1000 * this.dt)),
      y: Math.min(2, Math.max(-2, this.phisicMap.flowY[canBlock.mapId] * 1000 * this.dt)),
    }
    return deltaCoords;
  };
  phisicShiftingBlurFillKernel(kernel, kernelHalfSize, shiftDeltaCoords) {
    const kernelSize = ((kernelHalfSize * 2)+1);
    for (let kY = -kernelHalfSize; kY <= kernelHalfSize; kY++) {
      for (let kX = -kernelHalfSize; kX <= kernelHalfSize; kX++) {
        const kernelCoords = {
          x: kX + kernelHalfSize,
          y: kY + kernelHalfSize,
        };
        const shiftCoords = {
          x: kX + shiftDeltaCoords.x,
          y: kY + shiftDeltaCoords.y,
        }
        const kernelId = kernelCoords.y * kernelSize + kernelCoords.x;
        const kernelWeight = 1 / (1 + 10 * Math.sqrt(shiftCoords.x**2 + shiftCoords.y**2));
        kernel[kernelId] = kernelWeight;
      }
    }
  }
  phisicShiftingBlurMap(phisicMap, kernelHalfSize) {
    const bluredMap = new Float64Array(this.phisicCache.fieldSize.width*this.phisicCache.fieldSize.height);
    const kernel = this.phisicShiftingBlurBuildKernel(kernelHalfSize);
    this.phisicCache.canBlocks.forEach((canBlock)=>{
      const shiftDeltaCoords = this.phisicShiftingBlurCalcShiftDeltaCoords(canBlock);
      this.phisicShiftingBlurFillKernel(kernel, kernelHalfSize, shiftDeltaCoords);
      const kernelMask = canBlock.kernelMask[kernelHalfSize];
      let valueSum = 0;
      let samplesWeightSum = 0;
      kernelMask.forEach((kernelMaskElement) => {
        const sampleWeight = kernel[kernelMaskElement.kernelId];
        valueSum += phisicMap[kernelMaskElement.mapId] * sampleWeight;

        samplesWeightSum += sampleWeight;
      });
      const mapCId = canBlock.mapId;
      if (samplesWeightSum) {
        bluredMap[mapCId] = valueSum / samplesWeightSum;
      } else {
        console.warn('no samples!');
        bluredMap[mapCId] = phisicMap[mapCId];
      }
    })
    return bluredMap;
  }
  phisicShiftingBlurIteration() {
    this.phisicMap.thermo = this.phisicShiftingBlurMap(this.phisicMap.thermo, this.phisicCache.kernelHalfSize.shiftingThermo);
  }
  walkStep(phisicCoords, isDown = false) {
    const mapCId = this.getMapId(phisicCoords);
    const cRelPress = this.phisicMap.relPress[mapCId];
    const kernelHalfSize = 1;
    const downCaseMultiplier = isDown ? -1 : 1;
    let bestRelPress = cRelPress;
    let bestCoords = null;
    for (let kY = -kernelHalfSize; kY <= kernelHalfSize; kY++) {
      for (let kX = -kernelHalfSize; kX <= kernelHalfSize; kX++) {
        const sampleCoords = {
          x: phisicCoords.x + kX,
          y: phisicCoords.y + kY,
        }
        if ((sampleCoords.x >=0) &&
            (sampleCoords.y >=0) &&
            (sampleCoords.x < this.phisicCache.fieldSize.width) &&
            (sampleCoords.y < this.phisicCache.fieldSize.height) ) {

          const mapSId = this.getMapId(sampleCoords);
          const sRelPress = this.phisicMap.relPress[mapSId];
          if ((sRelPress * downCaseMultiplier) > (bestRelPress * downCaseMultiplier)) {
            // (enless-looped) self complare will not be passed only until strict complare is used!
            let isRelPressable = this.checkPointEntity(sampleCoords, this.phisicCache.entityRects.can) &&
                                !this.checkPointEntity(sampleCoords, this.phisicCache.entityRects.root) &&
                                !this.checkPointEntity(sampleCoords, this.phisicCache.entityRects.pot);
            if (isRelPressable) {
              bestRelPress = sRelPress;
              bestCoords = sampleCoords;
            }
          }
        }
      }
    }
    return bestCoords;
  }
  walk(phisicCenterCoords, maxStepsPerSide) {
    const steps = [phisicCenterCoords];
    let currentStepCoords = phisicCenterCoords;

    for (let s = 0; s < maxStepsPerSide; s++) {
      const newStepCoords = this.walkStep(currentStepCoords, false);
      if (newStepCoords) {
        //steps.unshift(newStepCoords);
        steps.push(newStepCoords);
        currentStepCoords = newStepCoords;
      } else {
        break;
      }
    }
    currentStepCoords = phisicCenterCoords;

    for (let s = 0; s < maxStepsPerSide; s++) {
      const newStepCoords = this.walkStep(currentStepCoords, true);
      if (newStepCoords) {
        steps.unshift(newStepCoords);
        //steps.push(newStepCoords);
        currentStepCoords = newStepCoords;
      } else {
        break;
      }
    }
    return steps;
  }
  phisicWalkIteration() {
    const stochoItersPerSec = 1000 * this.resolutionFactor**2;
    const stochoIters = stochoItersPerSec * this.dt;

    for (let s = 0; s < stochoIters; s++) {
      const randomCanBlockId = Math.floor(Math.random() * this.phisicCache.canBlocks.length);
      const canBlock = this.phisicCache.canBlocks[randomCanBlockId];
      const coords = canBlock.coords;

      const maxStepsPerSide = 10 * this.resolutionFactor;
      const steps = this.walk(coords, maxStepsPerSide);
      if (steps.length > 1) {
        let diffRelPressSum = 0;
        for (let stepId = 0; stepId < steps.length - 1; stepId++) {
          const nextStepId = (stepId + 1) % steps.length;
          const currentStepCoords = steps[stepId];
          const nextStepCoords = steps[nextStepId];
          const mapCurrentId = this.getMapId(currentStepCoords);
          const mapNextId = this.getMapId(nextStepCoords);
          const diffRelPress = this.phisicMap.relPress[mapNextId] - this.phisicMap.relPress[mapCurrentId];
          diffRelPressSum += diffRelPress;
        }
        const diffRelPressMean = diffRelPressSum / steps.length;

        const isSochoAllow = diffRelPressMean > Math.random() / 2;
        if (isSochoAllow) {
          const wrappedFirstStepCoords = steps[0];
          const mapWrappedFirstId = this.getMapId(wrappedFirstStepCoords);
          const wrappedFirstStep = {
            thermo: this.phisicMap.thermo[mapWrappedFirstId],
            relPress: this.phisicMap.relPress[mapWrappedFirstId],
          }
          const wrappedLastStepCoords = steps[steps.length - 1];
          const mapWrappedLastId = this.getMapId(wrappedLastStepCoords);

          for (let stepId = 0; stepId < steps.length - 1; stepId++) {
            const nextStepId = (stepId + 1) % steps.length;
            const currentStepCoords = steps[stepId];
            const nextStepCoords = steps[nextStepId];
            const mapCurrentId = this.getMapId(currentStepCoords);
            const mapNextId = this.getMapId(nextStepCoords);
            this.phisicMap.thermo[mapCurrentId] = this.phisicMap.thermo[mapNextId];
            //this.phisicMap.relPress[mapCurrentId] = this.phisicMap.relPress[mapNextId];

          }

          this.phisicMap.thermo[mapWrappedLastId]= wrappedFirstStep.thermo;
          //this.phisicMap.relPress[mapWrappedLastId]= wrappedFirstStep.relPress;
        }
      }
    }

  }
  phisicThermoRadiateIteration() {
    this.phisicCache.canBlocks.forEach((canBlock)=>{

      this.phisicMap.thermo[canBlock.mapId] = this.phisicMap.thermo[canBlock.mapId] * 0.999 + this.tweakable.temperatureKelOutside * 0.001;

    });
  }
  spoilThermo() {
    const stochoItersPerSec = 1000 * this.resolutionFactor**2;
    const stochoIters = stochoItersPerSec * this.dt;

    for (let s = 0; s < stochoIters; s++) {
      const randomCanBlockId = Math.floor(Math.random() * this.phisicCache.canBlocks.length);
      const canBlock = this.phisicCache.canBlocks[randomCanBlockId];
      this.phisicMap.thermo[canBlock.mapId] += Math.random() * 1000 - 500;
    }

  }
}
module.exports = PhisicSimulator;