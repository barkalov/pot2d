class asyncInitable {
  isInited = false;
  initedPromiseResolve = null;
  initedPromise = new Promise((resolve) => {
    this.initedPromiseResolve = resolve;
  });
  async init() {
    if (this.isInited) {
      throw new Error('already ininted');
    } else {
      this.initedPromiseResolve(this);
      isInited = true;
      return await this.initedPromise;
    }
  }
  static async create(...newArgs) {
    const instance = new this(...newArgs);
    return await instance.init();
  }
}
class SimApi extends asyncInitable {
  sim = null;
  constructor(sim) {
    this.sim = sim;
  }
  async init() {
    return await super.init();
  };
  heaterImpactor = {
    setValue: () => {

    },
  }
}