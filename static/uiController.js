import { Radio } from './uiControl/radio.js';
import { Button } from './uiControl/button.js';
import { Range } from './uiControl/range.js';
export class UiController {
  sim = null;
  renderer = null;
  uiControlsHolder = null;
  modeSelector = null;
  resetButton = null;
  spoilThermoButton = null;
  radiateRange = null;
  relPressRange = null;
  heaterRange = null;
  constructor(sim, renderer, {uiControlsHolder}) {
    this.sim = sim;
    this.renderer = renderer;
    this.uiControlsHolder = uiControlsHolder;
    this.modeSelectorRadio = new Radio(this.uiControlsHolder, 'mode', this.renderer.modes, 0);
    this.modeSelectorRadio.onChangeCallback = (mode, modeId) => {
      this.renderer.modeId = modeId;
      console.log('mode changed to', mode);
    };
    this.renderer.modeId = 0;
    this.resetButton = new Button(this.uiControlsHolder, 'reset');
    this.resetButton.onClickCallback = () => {
      console.log('RESET');
      this.sim.reset();
    }
    this.spoilThermoButton = new Button(this.uiControlsHolder, 'spoilThermo');
    this.spoilThermoButton.onClickCallback = () => {
      this.sim.spoilThermo();
    }
    this.radiateRange = new Range(this.uiControlsHolder, 'radiate', {min: -20, max: 50}, 16);
    this.radiateRange.onChangeCallback = (value) => {
      this.sim.tweakable.temperatureKelOutside = value + 272;
    }
    this.radiateRange.value = 16 + 272;
    this.relPressRange = new Range(this.uiControlsHolder, 'relPress', {min: -10000, max: 10000}, 0);
    this.relPressRange.onChangeCallback = (value) => {
      this.sim.tweakable.relPressPower = value;
    }
    this.relPressRange.value = 0;
    this.heaterRange = new Range(this.uiControlsHolder, 'heater', {min: -10000, max: 10000}, 0);
    this.heaterRange.onChangeCallback = (value) => {
      this.sim.tweakable.heaterPower = value;
    }
    this.heaterRange.value = 0;
  }

}