export class Button {
  name = null;
  holder = null;
  container = null;
  button = null;

  onClickCallback = null;
  constructor(holder, name) {
    this.name = name;
    this.holder = holder;
    this.buildDom();
  }
  buildDom() {
    this.container = document.createElement('div');
    this.button = document.createElement('button');
    this.button.innerText = this.name;
    this.container.appendChild(this.button);
    this.button.onclick = () => {
      this.click();
    };
    this.holder.appendChild(this.container);
  };
  click() {
    if (this.onClickCallback) {
      this.onClickCallback();
    }
  }
}