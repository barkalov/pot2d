export class Range {
  name = null;
  scale = {
    min: null,
    max: null,
  };
  holder = null;
  container = null;
  input = null;
  gauge = null;
  value = null;
  label = null;

  onChangeCallback = null;
  constructor(holder, name, scale, value) {
    this.name = name;
    this.scale = scale;
    this.holder = holder;
    this.value = value;
    this.buildDom();
  }
  buildDom() {
    this.container = document.createElement('div');
    this.label = document.createElement('div');
    this.label.innerText = this.name;
    this.container.appendChild(this.label);
    this.input = document.createElement('input');
    this.gauge = document.createElement('div');
    this.input.type = 'range';
    console.log('aasd');
    this.input.value = this.value;
    this.input.min = this.scale.min;
    this.input.max = this.scale.max;
    this.container.appendChild(this.input);
    this.container.appendChild(this.gauge);
    this.gauge.innerText = this.value.toFixed(2);
    this.input.onchange = () => {
      const value = parseFloat(this.input.value);
      this.change(value);
    };
    this.holder.appendChild(this.container);
  };
  change(value) {
    this.value = value;
    this.input.value = this.value;
    this.gauge.innerText = this.value.toFixed(2);
    if (this.onChangeCallback) {
      this.onChangeCallback(this.value);
    }
  }
}