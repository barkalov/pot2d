export class Radio {
  variants = null;
  selectedId = null;
  holder = null;
  container = null;
  buttons = null;
  name = null;
  label = null;

  onChangeCallback = null;
  constructor(holder, name, variants, selectedId) {
    this.name = name;
    this.variants = variants;
    this.selectedId = selectedId;
    this.holder = holder;
    this.buildDom();
  }
  buildDom() {
    this.container = document.createElement('div');
    this.label = document.createElement('div');
    this.label.innerText = this.name;
    this.container.appendChild(this.label);
    this.buttons = [];
    this.variants.forEach((variantName, variantId)=>{
      const button = document.createElement('button');
      button.innerText = variantName;
      this.container.appendChild(button);
      button.onclick = this.onclickFnFactory(variantId);
      this.buttons.push(button);
    })
    this.holder.appendChild(this.container);
  };
  onclickFnFactory(selectedId) {
    return () => {
      this.change(selectedId);
    }
  };
  change(selectedId) {
    this.selectedId = selectedId;
    if (this.onChangeCallback) {
      const selectedVariantName = this.variants[selectedId];
      this.onChangeCallback(selectedVariantName, selectedId);
    }
  }
}