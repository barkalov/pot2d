import { Radio } from './uiControl/radio.js';
export class Renderer {
  sim = null;
  phisicCanvas = null;
  phisicCtx = null;
  statusInfo = null;
  blockOutliner = null;
  blockOutlinerInfo = null;
  statusInfo = null;
  outlinedBlock = null; // may be an object {x, y} at runtime
  scaleFactor = 10;
  constructor(sim, {canvasContainer, blockOutliner, blockOutlinerInfo, statusInfo, phisicCanvas}) {
    console.log('hey');
    this.sim = sim;
    this.phisicCanvas = phisicCanvas;
    this.phisicCtx = this.phisicCanvas.getContext('2d');
    this.canvasContainer = canvasContainer;
    this.blockOutliner = blockOutliner;
    this.blockOutlinerInfo = blockOutlinerInfo;
    this.statusInfo = statusInfo;
    this.modes = ['schema', 'thermo', 'relPress', 'flowX', 'flowY'];
    this.modeId = 0;
    this.updateCanvasSize();
    this.renderPhisicCanvas();
    this.updateBlockOutliner();
    this.updateBlockOutlinerInfo();
    this.updateStatusInfoText();
    this.canvasContainer.onclick = (e) =>{
      console.log('catchme', e);
      const boundingClientRect = this.canvasContainer.getBoundingClientRect();
      const relMouseCoords = {
        x: e.clientX - boundingClientRect.left,
        y: e.clientY - boundingClientRect.top
      }
      this.onMouseClick(relMouseCoords);
    }
    setInterval(()=>{
      this.renderPhisicCanvas();
      this.updateBlockOutlinerInfoText();
      this.updateStatusInfoText();
    }, 20);
  }
  updateCanvasSize() {

    this.phisicCanvas.width = this.sim.phisicCache.fieldSize.width;
    this.phisicCanvas.height = this.sim.phisicCache.fieldSize.height;

    const canvasContainerSize = {
      width: this.sim.phisicCache.fieldSize.width * this.scaleFactor,
      height: this.sim.phisicCache.fieldSize.height * this.scaleFactor,
    };

    this.canvasContainer.style.width = canvasContainerSize.width;
    this.canvasContainer.style.height = canvasContainerSize.height;

    const blockOutlinerSize = {
      width: this.scaleFactor,
      height: this.scaleFactor,
    };
    this.blockOutliner.style.width = blockOutlinerSize.width;
    this.blockOutliner.style.height = blockOutlinerSize.height;
    this.updateBlockOutliner();
  }
  getEntityPixelColor(phisicCoords) {
    const state = this.sim.getPointEntityState(phisicCoords);
    let pixel = [128, 128, 128, 128];
    if (state.can) {
      pixel = [128, 128, 128, 32];
    }
    if (state.root) {
      pixel[0] -= 128;
      pixel[1] += 64;
      pixel[2] -= 128;
      pixel[3] += 32;
    }
    if (state.pot) {
      pixel[0] -= 128;
      pixel[1] -= 128;
      pixel[2] -= 128;
      pixel[3] += 192;
    }

    if (state.heater) {
      pixel[0] += 224;
      pixel[1] -= 64;
      pixel[2] -= 64;
      pixel[3] = 255;
    }
    if (state.phUpDispencer) {
      pixel[0] = 0;
      pixel[1] = 0;
      pixel[2] = 64;
      pixel[3] = 255;
    }
    if (state.phDownDispencer) {
      pixel[0] = 64;
      pixel[1] = 0;
      pixel[2] = 0;
      pixel[3] = 255;
    }
    if (state.relPressInDispencer) {
      pixel[0] = 255;
      pixel[1] = 0;
      pixel[2] = 255;
      pixel[3] = 255;
    }
    if (state.relPressOutDispencer) {
      pixel[0] = 0;
      pixel[1] = 255;
      pixel[2] = 255;
      pixel[3] = 255;
    }
    if (state.fertDispencer) {
      pixel[0] = 0;
      pixel[1] = 64;
      pixel[2] = 0;
      pixel[3] = 255;
    }
    if (state.heaterSensor) {
      pixel[0] = 255;
      pixel[1] = 192;
      pixel[2] = 192;
      pixel[3] = 255;
    }
    if (state.phSensor) {
      pixel[0] = 192;
      pixel[1] = 192;
      pixel[2] = 255;
      pixel[3] = 255;
    }
    if (state.fertSensor) {
      pixel[0] = 192;
      pixel[1] = 255;
      pixel[2] = 192;
      pixel[3] = 255;
    }
    for (let c = 0; c < 4; c++) {
      pixel[c] = Math.max(0, Math.min(255, pixel[c]));

    }
    return pixel;
  }
  HsvatoRgba([h, s, v, a]) {
  let r, g, b, i, f, p, q, t;
  i = Math.floor(h * 6);
  f = h * 6 - i;
  p = v * (1 - s);
  q = v * (1 - f * s);
  t = v * (1 - (1 - f) * s);
  switch (i % 6) {
    case 0: r = v, g = t, b = p; break;
    case 1: r = q, g = v, b = p; break;
    case 2: r = p, g = v, b = t; break;
    case 3: r = p, g = q, b = v; break;
    case 4: r = t, g = p, b = v; break;
    case 5: r = v, g = p, b = q; break;
  }
  return [
    Math.round(r * 255),
    Math.round(g * 255),
    Math.round(b * 255),
    a
  ];
}
  getRelPressPixelColor(phisicCoords) {

    const mapId = this.sim.getMapId(phisicCoords);
    const relPress = this.sim.phisicMap.relPress[mapId];
    const pixel = this.HsvatoRgba([0.5 - relPress / 20, 1, 0.5, 255]);
    return pixel;
  }
  getThermoPixelColor(phisicCoords) {

    const mapId = this.sim.getMapId(phisicCoords);
    const temperatureK = this.sim.phisicMap.thermo[mapId];
    const temperatureC = temperatureK - 272;
    let hue = 1 - temperatureC / 10;
    hue %= 1;
    hue += 1;
    hue %= 1;
    const pixel = this.HsvatoRgba([hue, 1, 0.5, 255]);
    return pixel;
  }
  getFlowPixelColor(phisicCoords) {
    const mapId = this.sim.getMapId(phisicCoords);
    const flowX = this.sim.phisicMap.flowX[mapId];
    const flowY = this.sim.phisicMap.flowY[mapId];
    const pixel = [128 + 255 * flowX, 128 + 255 * flowY, 128, 255];
    return pixel;
  }

  renderPhisicCanvas() {
    const mode = this.modes[this.modeId];
    this.clearPhisicCanvas();
    if (mode === 'schema') {
      this.renderPhisicCanvasSchema();
    } else if (mode === 'thermo') {
      this.renderPhisicCanvasThermo();
      //this.renderPhisicCanvasSchema();
    } else if (mode === 'relPress') {
      this.renderPhisicCanvasRelPress();
    } else if (mode === 'flowX') {
      this.renderPhisicCanvasFlow();
    } else if (mode === 'flowY') {
      this.renderPhisicCanvasFlow();
    } else {
      console.warn('wrong mode');
    }
  }
  clearPhisicCanvas() {

    this.phisicCtx.clearRect(0, 0, this.sim.phisicCache.fieldSize.width, this.sim.phisicCache.fieldSize.height);
  }
  renderPhisicCanvasSchema() {

    const imageData = this.phisicCtx.getImageData(0, 0, this.sim.phisicCache.fieldSize.width, this.sim.phisicCache.fieldSize.height);
    const channelsCount = 4; // constant because always&only rgba
    for (let phisicCanvasY = 0; phisicCanvasY < this.sim.phisicCache.fieldSize.height; phisicCanvasY++) {
      for (let phisicCanvasX = 0; phisicCanvasX < this.sim.phisicCache.fieldSize.width; phisicCanvasX++) {
        const phisicCoords = {
          x: phisicCanvasX,
          y: phisicCanvasY
        }
        const pixel = this.getEntityPixelColor(phisicCoords);
        const imageDataId = (phisicCanvasY * this.sim.phisicCache.fieldSize.width + phisicCanvasX) * channelsCount;
        for (let c = 0; c < channelsCount; c++) {
          imageData.data[imageDataId + c] += pixel[c];
        }
      }
    }
    this.phisicCtx.putImageData(imageData, 0, 0);
  }
  renderPhisicCanvasThermo() {

    const imageData = this.phisicCtx.createImageData(this.sim.phisicCache.fieldSize.width, this.sim.phisicCache.fieldSize.height);
    const channelsCount = 4; // constant because always&only rgba
    for (let phisicCanvasY = 0; phisicCanvasY < this.sim.phisicCache.fieldSize.height; phisicCanvasY++) {
      for (let phisicCanvasX = 0; phisicCanvasX < this.sim.phisicCache.fieldSize.width; phisicCanvasX++) {
        const phisicCoords = {
          x: phisicCanvasX,
          y: phisicCanvasY
        }
        const pixel = this.getThermoPixelColor(phisicCoords);
        const imageDataId = (phisicCanvasY * this.sim.phisicCache.fieldSize.width + phisicCanvasX) * channelsCount;
        for (let c = 0; c < channelsCount; c++) {
          imageData.data[imageDataId + c] = pixel[c];
        }
      }
    }
    this.phisicCtx.putImageData(imageData, 0, 0);
  }
  renderPhisicCanvasRelPress() {

    const imageData = this.phisicCtx.createImageData(this.sim.phisicCache.fieldSize.width, this.sim.phisicCache.fieldSize.height);
    const channelsCount = 4; // constant because always&only rgba
    for (let phisicCanvasY = 0; phisicCanvasY < this.sim.phisicCache.fieldSize.height; phisicCanvasY++) {
      for (let phisicCanvasX = 0; phisicCanvasX < this.sim.phisicCache.fieldSize.width; phisicCanvasX++) {
        const phisicCoords = {
          x: phisicCanvasX,
          y: phisicCanvasY
        }
        const pixel = this.getRelPressPixelColor(phisicCoords);
        const imageDataId = (phisicCanvasY * this.sim.phisicCache.fieldSize.width + phisicCanvasX) * channelsCount;
        for (let c = 0; c < channelsCount; c++) {
          imageData.data[imageDataId + c] = pixel[c];
        }
      }
    }
    this.phisicCtx.putImageData(imageData, 0, 0);
  }
  renderPhisicCanvasFlow() {
    const imageData = this.phisicCtx.createImageData(this.sim.phisicCache.fieldSize.width, this.sim.phisicCache.fieldSize.height);
    const channelsCount = 4; // constant because always&only rgba
    for (let phisicCanvasY = 0; phisicCanvasY < this.sim.phisicCache.fieldSize.height; phisicCanvasY++) {
      for (let phisicCanvasX = 0; phisicCanvasX < this.sim.phisicCache.fieldSize.width; phisicCanvasX++) {
        const phisicCoords = {
          x: phisicCanvasX,
          y: phisicCanvasY
        }
        const pixel = this.getFlowPixelColor(phisicCoords);
        const imageDataId = (phisicCanvasY * this.sim.phisicCache.fieldSize.width + phisicCanvasX) * channelsCount;
        for (let c = 0; c < channelsCount; c++) {
          imageData.data[imageDataId + c] = pixel[c];
        }
      }
    }
    this.phisicCtx.putImageData(imageData, 0, 0);
  }
  onMouseClick({x, y}) {
    this.outlineBlockFromMouseCoords({x, y});
    this.updateBlockOutliner();
    this.updateBlockOutlinerInfo();
  }
  updateBlockOutliner() {
    if (this.outlinedBlock) {
      this.blockOutliner.style.display = 'block';
      this.blockOutliner.style.left = this.outlinedBlock.x * this.scaleFactor;
      this.blockOutliner.style.top = this.outlinedBlock.y * this.scaleFactor;
    } else {
      this.blockOutliner.style.display = 'none';
    }
  }
  updateBlockOutlinerInfo() {
    let backgroundColor = [255,255,255,255];
    let borderColor = [128,128,128,128];
    if (this.outlinedBlock) {
      borderColor = this.getEntityPixelColor(this.outlinedBlock);
      for (let c = 0; c < 3; c++) {
        backgroundColor[c] = Math.floor(borderColor[c] * 0.2 + 255 * 0.8)
      }
    }
    this.blockOutlinerInfo.style.backgroundColor = `rgba(${backgroundColor[0]}, ${backgroundColor[1]}, ${backgroundColor[2]}, ${backgroundColor[3]})`;
    this.blockOutlinerInfo.style.borderColor = `rgba(${borderColor[0]}, ${borderColor[1]}, ${borderColor[2]}, ${borderColor[3]})`;
    this.updateBlockOutlinerInfoText();
  }
  updateBlockOutlinerInfoText() {
    let text = 'nothing selected';
    if (this.outlinedBlock) {
      const state = this.sim.getPointEntityState(this.outlinedBlock);
      text = Object.entries(state).filter(([stateKey, stateValue]) => stateValue).map(([stateKey, stateValue]) => stateKey).join(', ');
      const mapId = this.sim.getMapId(this.outlinedBlock);
      const mapValues = {
        thermo: this.sim.phisicMap.thermo[mapId],
        relPress: this.sim.phisicMap.relPress[mapId],
        flowX: this.sim.phisicMap.flowX[mapId],
        flowY: this.sim.phisicMap.flowY[mapId],
      }
      text += '\r\n';
      text += `thermo: ${mapValues.thermo.toFixed(2)} K ( ${(mapValues.thermo - 272).toFixed(2)} C)`;
      text += '\r\n';
      text += `relPress: ${mapValues.relPress.toFixed(2)}`;
      text += '\r\n';
      text += `flowX: ${mapValues.flowX.toFixed(2)}`;
      text += '\r\n';
      text += `flowY: ${mapValues.flowY.toFixed(2)}`;
    }
    this.blockOutlinerInfo.innerText = text;
  }
  outlineBlockFromMouseCoords({x, y}) {
    const phisicCoords = {
      x: Math.floor(x / this.scaleFactor),
      y: Math.floor(y / this.scaleFactor),
    }

    if ((phisicCoords.x < 0) ||
        (phisicCoords.y < 0) ||
        (phisicCoords.x > this.sim.phisicCache.fieldSize.width  - 1) ||
        (phisicCoords.y > this.sim.phisicCache.fieldSize.height - 1)) {
         this.outlinedBlock = null;
       } else {
        this.outlinedBlock = phisicCoords;
       }
  }

  updateStatusInfoText() {
    let text = '';
    text += `statistic:`;
    text += '\r\n';
    if (typeof this.sim.statistic.cycleDt === 'number') {
      text += `cycleDt: ${this.sim.statistic.cycleDt.toFixed(3)}`;
    } else {
      text += 'no cycleDt';
    }
    text += '\r\n';
    if (typeof this.sim.statistic.dutyDt === 'number') {
      text += `dutyDt: ${this.sim.statistic.dutyDt.toFixed(3)}`;
    } else {
      text += 'no dutyDt';
    }
    text += '\r\n';
    if (typeof this.sim.statistic.smooth.cycleDt === 'number') {
      text += `smooth.cycleDt: ${this.sim.statistic.smooth.cycleDt.toFixed(3)}`;
    } else {
      text += 'no smooth.cycleDt';
    }
    text += '\r\n';
    if (typeof this.sim.statistic.smooth.dutyDt === 'number') {
      text += `smooth.dutyDt: ${this.sim.statistic.smooth.dutyDt.toFixed(3)}`;
    } else {
      text += 'no smooth.dutyDt';
    }
    text += '\r\n';
    this.statusInfo.innerText = text;
  }








}
