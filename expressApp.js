'use strict';
const express = require('express');
const app = express();
app.set('view engine', 'html');

const router = express.Router();
router.get('/', function(req, res, next) {
  console.info(req.connection.remoteAddress, 'answering to a GET request');
  res.send('ok');
});
app.use('/', express.static(__dirname + '/static'));
app.use('/router', router);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  console.warn('Error:', err);  
  res.send('error');
});

module.exports = app;
