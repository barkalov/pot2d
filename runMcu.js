const mqtt = require('mqtt');
const wrappedEntrypoint = require('./mcu/fs/mom.js');
const { runEntrypoint } = require('./mcu/mongooseosEmulator/emulate.js');

async function runMcu() {
  console.log('conecting to mqtt...');
  const mqttClient = mqtt.connect('mqtt://localhost');
  await new Promise((resolve)=>{
    mqttClient.on('connect', () => {
      console.log('conected to mqtt!');
      resolve();
    });
  });
  const {mom} = runEntrypoint(wrappedEntrypoint, mqttClient);
  return {mom};
};
module.exports = runMcu;
