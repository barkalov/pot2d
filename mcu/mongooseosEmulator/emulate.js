const nodePath = require('path');
const Mqtt = require('./modules/mqtt.js');
const Timer = require('./modules/timer.js');
const print = require('./modules/print.js');
const mom = {};
let MQTT = null; // will be setted in wrappedEntrypoint
function loadFactory(dirname) {
  function load(path) {
    require(nodePath.join(dirname, path));
  }
  return load;
}
function runEntrypoint(wrappedEntrypoint, mqttClient) {
  return wrappedEntrypoint(mqttClient);
}
function entrypointWrapper(mjsProgramAsCallback,  dirname) {
  function wrappedEntrypoint(mqttClient) {
    if (MQTT) {
      throw new Error('MQTT must not be resolved at entrypointWrapper run');
    }
    MQTT = new Mqtt(mqttClient);
    const load = loadFactory(dirname);
    mjsProgramAsCallback({mom}, {load, MQTT, Timer, print});
    return {mom};
  };
  return wrappedEntrypoint;
}
function loadWrapper(mjsProgramAsCallback, dirname) {
  if (!MQTT) {
    throw new Error('MQTT must be resolved first (via running entrypointWrapper) before any load')
  }
  const load = loadFactory(dirname);
  mjsProgramAsCallback({mom}, {load, MQTT, Timer, print});
}

module.exports = {runEntrypoint, entrypointWrapper, loadWrapper, mom};