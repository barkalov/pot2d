function print(string) {
  if (typeof string !== 'string') {
    console.warn(`${typeof string} is bad type to print`);
  } else {
    console.log(string);
  }
}
module.exports = print;