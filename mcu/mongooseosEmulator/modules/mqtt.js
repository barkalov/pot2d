class Mqtt {
  mqttClient = null;
  subs = {};
  constructor(mqttClient) {
    this.mqttClient = mqttClient;
    this.mqttClient.on('message', (topic, messageBuffer) => {
      this.this._onMessage(topic, messageBuffer);
    });
  }
  _onMessage(topic, messageBuffer) {
    const topicTokens = topic.split('/');
    const message = messageBuffer.toString();
    console.log(`get topic: ${topic}, message: ${message}`);
    const topicExpressions = Object.keys(this.subs);
    topicExpressions.forEach((topicExpression) => {
      const topicExpressionTokens = topicExpression.split('/');
      const maxTokensLength = Math.max(topicTokens.length, topicExpressionTokens.length)
      let istopicExpressionFit = true;
      for (let tokenId = 0; tokenId < maxTokensLength; tokenId++) {
        const topicExpressionToken = topicExpressionTokens[tokenId];
        const topicToken = topicTokens[topicId];
        if (topicToken === undefined) {
          istopicExpressionFit = false;
          break;
        } else if (topicExpressionToken === undefined) {
          istopicExpressionFit = false;
          break;
        } else if (topicExpressionToken === '#') {
          break;
        } else if (topicExpressionToken === '+') {
          // go next iteration
        } else if (topicExpressionToken !== topicToken) {
          istopicExpressionFit = false;
          break;
        }
      }
      if (istopicExpressionFit) {
        let sub = this.subs[topicExpression]
        sub.handler(this, topic, message, sub.userdata);
      }
    });
  }
  pub(topic, message, qos, retain) {
    console.log(`pub topic: ${topic}, message: ${message}`);
    this.mqttClient.publish(topic, message);
  }
  sub(topicExpression, handler, userdata) {
    console.log(`sub topic: ${topicExpression}`);
    this.mqttClient.subscribe(topicExpression, (err, granted) => {
      if (err) {
        throw err;
      } else {
        granted.forEach((grant) => {
          const topicExpression = grant.topic;
          this.subs[topicExpression] = {handler, userdata};
        });
      };
    });

  }

}

module.exports = Mqtt;