class Timer {
  set(milliseconds, flags, handler, userdata) {
    if (flags) {
      return setInterval(() => {
        handler(userdata);
      }, milliseconds);
    } else {
      return setTimeout(() => {
        handler(userdata);
      }, milliseconds);
    }
  }
  REPEAT = 1; //flag const
}
module.exports = new Timer();