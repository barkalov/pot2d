const mongooseosEmulateLoadWrapper = require('../mongooseosEmulator/emulate.js').loadWrapper; // remove when porting to mongooseos
mongooseosEmulateLoadWrapper(({mom}, {load, MQTT, Timer, print}) => { // remove when porting to mongooseos

  mom.device.impactor.fertDispencer.setValueWorkload = function(value, endCallback, endCallbackUserdata) {
    print('fertDispencer.setValueWorkload started');
    let userdata = {endCallback:endCallback, endCallbackUserdata: endCallbackUserdata, value: value};
    Timer.set(3000, 0, function(userdata) {
      userdata.endCallback(null, userdata.value, userdata.endCallbackUserdata);
      print('fertDispencer.setValueWorkload ended');
    }, userdata);
  }

}, __dirname); // remove when porting to mongooseos