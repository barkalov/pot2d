const mongooseosEmulateLoadWrapper = require('../mongooseosEmulator/emulate.js').loadWrapper; // remove when porting to mongooseos
mongooseosEmulateLoadWrapper(({mom}, {load, MQTT, Timer, print}) => { // remove when porting to mongooseos

  ////
  //// device prototypes
  ////


  //
  // mom.prototype.ltboDevice
  //

  mom.prototype.ltboDevice = {
    isBusy: false,
    currentLtboId: -1, // current ongoing long-term blocking operation id, to ban old callbacks (that may be awakingly fired after no-actibity-timeout is already finalize this task)
    maxTimeoutBeforeError: null, // to be specified in deeper level
    clone: function() {
      let clone = Object.create(this);
      return clone;
    },
    beginLongTermBlockingOperation: function(inactivityTimeoutCallback, inactivityTimeoutCallbackUserdata) {
      if (this.isBusy) {
        return false;
      } else {
        this.isBusy = true;
        this.currentLtboId = this.currentLtboId + 1;
        this.currentLtboId = this.currentLtboId % (256*256); // wrapparound
        let userdata = {that: this, currentLtboId: this.currentLtboId}; // need to be passed as an userdata, when porting on mongoose os, because it may dissapear (with entire scope, where it declared) at time, when timer is fired
        Timer.set(this.maxTimeoutBeforeError, 0, function(userdata) {
          let isLtboEndSuccessful = userdata.that.endLongTermBlockingOperation(userdata.currentLtboId);
          if (isLtboEndSuccessful) {
            inactivityTimeoutCallback(inactivityTimeoutCallbackUserdata);
          }
        }, userdata);
        return this.currentLtboId;
      }
    },
    endLongTermBlockingOperation: function(currentLtboId) {
      if (!this.isBusy || (currentLtboId !== this.currentLtboId)) {
        return false;
      } else {
        this.isBusy = false;
        return true;
      }
    },
  };

  //
  // mom.prototype.ltboSensorDevice
  //

  mom.prototype.ltboSensorDevice = mom.prototype.ltboDevice.clone();
  mom.prototype.ltboSensorDevice.topic = {
    statusValue: null, // to be specified in deeper level
    statusError: null, // to be specified in deeper level
  };
  mom.prototype.ltboSensorDevice.getValueWorkload = null; // to be specified in deeper level
  mom.prototype.ltboSensorDevice.maxTimeoutBeforeError = 10 * 1000;
  mom.prototype.ltboSensorDevice.clone = function() {
    let clone = Object.create(this);
    clone.topic = Object.create(clone.topic);
    return clone;
  };
  mom.prototype.ltboSensorDevice.sendMqttStatusValue = function(value) {
    let stringValue = value.toString();
    MQTT.pub(this.topic.statusValue, stringValue);
  };
  mom.prototype.ltboSensorDevice.sendMqttStatusError = function(message) {
    MQTT.pub(this.topic.statusError, message);
  };
  mom.prototype.ltboSensorDevice.iteration = function() {
    let inactivityTimeoutCallbackUserdata = {that: this};
    let inactivityTimeoutCallback = function (userdata) {
      userdata.that.sendMqttStatusError('no activity too long, while awaiting ltbo become done');
    };
    let currentLtboId = this.beginLongTermBlockingOperation(inactivityTimeoutCallback, inactivityTimeoutCallbackUserdata); // need to be passed as an userdata, when porting on mongoose os, because it may dissapear (with entire scope, where it declared) at time, when timer is fired
    if (currentLtboId === false) {
      this.sendMqttStatusError('ltbo is busy, something else in ltbo domain is doing already');
    } else { // LTBO is allowed and started
      let userdata = {that: this, currentLtboId: currentLtboId};
      this.getValueWorkload(function(error, value, userdata) {
        let isLtboEndSuccessful = userdata.that.endLongTermBlockingOperation(userdata.currentLtboId);
        if (isLtboEndSuccessful) {
          if (error) {
            userdata.that.sendMqttStatusError(error);
          } else {
            userdata.that.sendMqttStatusValue(value);
          }
        }
      }, userdata);
    }
  }

  //
  // mom.prototype.ltboImpactorDevice
  //

  mom.prototype.ltboImpactorDevice = mom.prototype.ltboDevice.clone();
  mom.prototype.ltboImpactorDevice.topic = {
    controlValue: null, // to be specified in deeper level
    statusValue: null, // to be specified in deeper level
    statusError: null, // to be specified in deeper level
  };
  mom.prototype.ltboImpactorDevice.setValueWorkload = null; // to be specified in deeper level
  mom.prototype.ltboImpactorDevice.bounds = {
    min: null, // to be specified in deeper level
    max: null, // to be specified in deeper level
  };
  mom.prototype.ltboImpactorDevice.maxTimeoutBeforeError = 10 * 1000;
  mom.prototype.ltboImpactorDevice.clone = function() {
    let clone = Object.create(this);
    clone.topic = Object.create(clone.topic);
    clone.bounds = Object.create(clone.bounds);
    return clone;
  };
  mom.prototype.ltboImpactorDevice.sendMqttStatusValue = function(value) {
    let stringValue = value.toString();
    MQTT.pub(this.topic.statusValue, stringValue);
  };
  mom.prototype.ltboImpactorDevice.sendMqttStatusError = function(message) {
    MQTT.pub(this.topic.statusError, message);
  };
  mom.prototype.ltboImpactorDevice.receiveMqttControlValue = function(stringValue) {
    let value = parseFloat(stringValue);
    if ((value < this.bounds.min) || (value > this.bounds.max)) {
      this.sendMqttStatusError('value is out of bounds');
    } else {
      this.setValue(value);
    }
  };
  mom.prototype.ltboImpactorDevice.setValue = function(value) {
    let inactivityTimeoutCallbackUserdata = {that: this};
    let inactivityTimeoutCallback = function (userdata) {
      userdata.that.sendMqttStatusError('no activity too long, while awaiting ltbo become done');
    }
    let currentLtboId = this.beginLongTermBlockingOperation(inactivityTimeoutCallback, inactivityTimeoutCallbackUserdata); // need to be passed as an userdata, when porting on mongoose os, because it may dissapear (with entire scope, where it declared) at time, when timer is fired
    if (currentLtboId === false) {
      this.sendMqttStatusError('ltbo is busy, something else in ltbo domain is doing already');
    } else { // LTBO is allowed and started
      mom.setValueCallbackUserdataWorkload = {that: this, currentLtboId: currentLtboId};
      mom.setValueCallbackWorkload = function (error, value, userdata) {
        let isLtboEndSuccessful = userdata.that.endLongTermBlockingOperation(userdata.currentLtboId);
        if (isLtboEndSuccessful) {
          if (error) {
            userdata.that.sendMqttStatusError(error);
          } else {
            userdata.that.sendMqttStatusValue(value);
          }
        }
      }
      this.setValueWorkload(value, mom.setValueCallbackWorkload, mom.setValueCallbackUserdataWorkload);
    }
  };
  mom.prototype.ltboImpactorDevice.init = function() {
    let userdata = {that: this};
    MQTT.sub(this.topic.controlValue, function(conn, topic, stringValue, userdata) {
      userdata.that.receiveMqttControlValue(stringValue);
    }, userdata);
  };

}, __dirname); // remove when porting to mongooseos