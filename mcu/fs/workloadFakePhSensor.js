const mongooseosEmulateLoadWrapper = require('../mongooseosEmulator/emulate.js').loadWrapper; // remove when porting to mongooseos
mongooseosEmulateLoadWrapper(({mom}, {load, MQTT, Timer, print}) => { // remove when porting to mongooseos

  mom.device.sensor.phSensor.getValueWorkload = function(endCallback, endCallbackUserdata) {
    print('phSensor.getValueWorkload started');
    let userdata = {endCallback:endCallback, endCallbackUserdata: endCallbackUserdata};
    Timer.set(500, 0, function(userdata) {
      let from = 5;
      let to = 9;
      let fraction = 2;
      let fractionFactor = 10 ** fraction;
      let value = Math.floor(Math.random() * (to - from) * fractionFactor) / fractionFactor + from;

      userdata.endCallback(null, value, userdata.endCallbackUserdata);
      print('phSensor.getValueWorkload ended');
    }, userdata);
  }

}, __dirname); // remove when porting to mongooseos