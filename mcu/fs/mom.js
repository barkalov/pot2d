const mongooseosEmulateEntrypointWrapper = require('../mongooseosEmulator/emulate.js').entrypointWrapper; // remove when porting to mongooseos
module.exports = mongooseosEmulateEntrypointWrapper(({mom}, {load, MQTT, Timer, print}) => { // remove when porting to mongooseos


  //////
  ////// device prototypes
  //////

  mom.prototype = {};
  load('ltboDevicePrototype.js');


  //////
  ////// devices
  //////

  mom.device = {};

  ////
  //// non-blocking sensor readings
  ////

  mom.device.sensor = {};

  //
  // temperature
  //

  mom.device.sensor.temperatureSensor = mom.prototype.ltboSensorDevice.clone();
  mom.device.sensor.temperatureSensor.topic.statusValue = '/mcu/sensor/temperature/status/value',
  mom.device.sensor.temperatureSensor.topic.statusError = '/mcu/sensor/temperature/status/error',
  mom.device.sensor.temperatureSensor.getValueWorkload = null; // will be loaded later in workload section

  //
  // ph
  //

  mom.device.sensor.phSensor = mom.prototype.ltboSensorDevice.clone();
  mom.device.sensor.phSensor.topic.statusValue = '/mcu/sensor/ph/status/value',
  mom.device.sensor.phSensor.topic.statusError = '/mcu/sensor/ph/status/error',
  mom.device.sensor.phSensor.getValueWorkload = null; // will be loaded later in workload section

  //
  // ppm
  //

  mom.device.sensor.ppmSensor = mom.prototype.ltboSensorDevice.clone();
  mom.device.sensor.ppmSensor.topic.statusValue = '/mcu/sensor/ppm/status/value',
  mom.device.sensor.ppmSensor.topic.statusError = '/mcu/sensor/ppm/status/error',
  mom.device.sensor.ppmSensor.getValueWorkload = null; // will be loaded later in workload section


  ////
  //// (actually only self-blocking) device (PWM-alike)
  ////

  mom.device.impactor = {};

  //
  // heat
  //

  mom.device.impactor.heatImpactor = mom.prototype.ltboImpactorDevice.clone();
  mom.device.impactor.heatImpactor.topic.controlValue = '/mcu/impactor/heat/control/value';
  mom.device.impactor.heatImpactor.topic.statusValue = '/mcu/impactor/heat/status/value';
  mom.device.impactor.heatImpactor.topic.statusError = '/mcu/impactor/heat/status/error';
  mom.device.impactor.heatImpactor.setValueWorkload = null; // will be loaded later in workload section
  mom.device.impactor.heatImpactor.bounds.min = 0;
  mom.device.impactor.heatImpactor.bounds.max = 1;


  ////
  //// true LTBO: long-term blocking devices
  ////

  //
  // fert
  //

  mom.device.impactor.fertDispencer = mom.prototype.ltboImpactorDevice.clone();
  mom.device.impactor.fertDispencer.topic.controlValue = '/mcu/dispenser/fert/control/value';
  mom.device.impactor.fertDispencer.topic.statusValue = '/mcu/dispenser/fert/status/value';
  mom.device.impactor.fertDispencer.topic.statusError = '/mcu/dispenser/fert/status/error';
  mom.device.impactor.fertDispencer.setValueWorkload = null; // will be loaded later in workload section
  mom.device.impactor.fertDispencer.bounds.min = 0;
  mom.device.impactor.fertDispencer.bounds.max = 1;

  //
  // phUp
  //

  mom.device.impactor.phUpDispencer = mom.prototype.ltboImpactorDevice.clone();
  mom.device.impactor.phUpDispencer.topic.controlValue = '/mcu/dispenser/phUp/control/value';
  mom.device.impactor.phUpDispencer.topic.statusValue = '/mcu/dispenser/phUp/status/value';
  mom.device.impactor.phUpDispencer.topic.statusError = '/mcu/dispenser/phUp/status/error';
  mom.device.impactor.phUpDispencer.setValueWorkload = null; // will be loaded later in workload section
  mom.device.impactor.phUpDispencer.bounds.min = 0;
  mom.device.impactor.phUpDispencer.bounds.max = 1;

  //
  // phDown
  //

  mom.device.impactor.phDownDispencer = mom.prototype.ltboImpactorDevice.clone();
  mom.device.impactor.phDownDispencer.topic.controlValue = '/mcu/dispenser/phDown/control/value';
  mom.device.impactor.phDownDispencer.topic.statusValue = '/mcu/dispenser/phDown/status/value';
  mom.device.impactor.phDownDispencer.topic.statusError = '/mcu/dispenser/phDown/status/error';
  mom.device.impactor.phDownDispencer.setValueWorkload = null; // will be loaded later in workload section
  mom.device.impactor.phDownDispencer.bounds.min = 0;
  mom.device.impactor.phDownDispencer.bounds.max = 1;

  ////
  //// workload
  ////

  load('workloadFakeTemperatureSensor.js');
  load('workloadFakePhSensor.js');
  load('workloadFakePpmSensor.js');
  load('workloadFakeHeatImpactor.js');
  load('workloadFakeFertDispencer.js');
  load('workloadFakePhUpDispencer.js');
  load('workloadFakePhDownDispencer.js');


  ////
  //// lifecycles
  ////

  mom.iteration = function() {
    mom.device.sensor.temperatureSensor.iteration();
    mom.device.sensor.phSensor.iteration();
    mom.device.sensor.ppmSensor.iteration();
  };

  mom.init = function() {
    mom.device.impactor.heatImpactor.init();
    mom.device.impactor.fertDispencer.init();
    mom.device.impactor.phUpDispencer.init();
    mom.device.impactor.phDownDispencer.init();
  };

  mom.start = function() {
    mom.init();
    Timer.set(5000, Timer.REPEAT, function () {
      mom.iteration();
    }, null)
  };

  //////
  ////// start
  //////
  print('mom is starting...');
  mom.start();

}, __dirname); // remove when porting to mongooseos

