////
//// this file runs on pure mcu only and no need to porting
////

//
// api and lib imports here
//

print('LOADING DEPENDENCIES...');
load('mqtt_api.js');
print('DEPENDENCIES LOADED OK!');

//
// mom entrypoint here
//

let mom = {};
print('STARTING MOM...');
load('mom.js');
print('MOM STATED OK!');