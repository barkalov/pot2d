'use strict';
const http = require('http');
const sioFactory = require('socket.io');
const os = require('os');
const config = require('./config.json');
const expressApp = require('./expressApp.js');
const Sim = require('./sim.js');

//// Get port from environment and store in Express.

const port = config.web.port;
expressApp.set('port', port);

//// Create HTTP(S) server.

const webServer = http.createServer(expressApp);

//// Create socket.io server.

const sio = sioFactory(webServer);

//// Create sim.

const sim = new Sim(sio);

//// Event listener for HTTP(S) webServer "error" event.

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error('port requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error('port is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

//// Event listener for HTTP(S) webServer "listening" event.

function onListening() {
  const addr = webServer.address().address;
  console.info('Listening port on ' + addr + ' (with os.hostname=' + os.hostname() + ')');
}

//// Listen on provided port, on all network interfaces.

webServer.on('error', onError);
webServer.on('listening', onListening);
webServer.listen(port);

module.exports = webServer;